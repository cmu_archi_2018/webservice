var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');
var morgan      = require('morgan');
var fs          = require('fs')
var path        = require('path')
//var cors        = require('cors');

// [CONFIGURE APP TO USE bodyParser]
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//app.use(cors())

// [CONFIGURE SERVER PORT]
var port = process.env.PORT || 8080;

// [LOADING CONFIG]
const config = require('./config/config');

app.use(morgan('combined'));
app.set('jwt-secret', config.secret);
app.set('jwt-secret-reset-password', config.secretResetPassword);

// [CONFIGURE ROUTER]

app.use(express.static(__dirname + '/build'));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.use('/api', require('./router/api'))

// [CONFIGURE DATABASE]
var db = mongoose.connection;
db.on('error', console.error);
db.once('open', function() {
    console.log("Connected to mongod server");
})
mongoose.connect(config.mongodbUri);

// // [HTTP RUN SERVER]
var server = app.listen(port, function(){
 console.log("Express server has started on port " + port)
});