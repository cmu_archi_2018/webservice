const crypto = require('crypto')
const config = require('../config/config')
const { tempPassword } = require('./misc')

var saltHashEncrypted = (password, salt) => {    
    var hashedPassword = crypto.createHmac('sha512', salt)
    .update(password)
    .digest('base64')

    return {
        salt: salt,
        password: hashedPassword
    }
}

module.exports = saltHashEncrypted