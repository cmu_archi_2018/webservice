var nodemailer = require('nodemailer');
var config = require('../config/config')

exports.resetPassword = (email, tempPassword, token) => {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: config.systemEmail,
          pass: config.systemEmailPassword
        }
      })

      var mailOptions = {
        from: config.systemEmail,
        to: email,
        subject: 'Team4 VoIP - Reset password',
        html: '<h1>Welcome</h1><p>Click below link,your password will be updated "' 
            + tempPassword + '"</P>'
            + '<p><a href="' + config.systemUrl + '/api/auth/recover?email='+ email
            + '&token=' + token
            + '">Reset password!!</a></p>'
    }      

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
 //           return false
        } else {
            console.log('Email sent: ' + info.response);
 //           return true
        }
    })

    return true
}