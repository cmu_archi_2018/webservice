const router = require('express').Router()
const controller = require('./controller')

router.post('/message', controller.sendMessage)
router.get('/message/:phoneNo', controller.pushMessage)
router.get('/conference/:phoneNo', controller.retrieveConference)
router.post('/conference', controller.registeConference)
router.patch('/conference', controller.updateConference)
router.delete('/conference', controller.deleteConference)
router.post('/callhistory', controller.saveCallHistory)
router.get('/callhistory', controller.retrieveCallHistory)
router.get('/messagehistory', controller.retrieveMessageHistory)
router.get('/servicehistory', controller.retrieveServiceHistory)
router.get('/dashboard', controller.retrieveDashboard)
//router.get('/message/:phoneNo', controller.getMessage)

module.exports = router