const User = require('../../../models/user')
const Message = require('../../../models/message')
const Conference = require('../../../models/conference')
const CallHistory = require('../../../models/callhistory')
const misc = require('../../../utils/misc')

/*
    POST /api/service/message
*/

exports.sendMessage = (req, res) => {
    const {phoneNo, email, receiveArray, message} = req.body

    const push = (calleePhoneNo) => {        
        if(phoneNo) {
            return Message.push(phoneNo, email, calleePhoneNo.phoneNo, calleePhoneNo.email, message, misc.lenBytr(message))
        } else {
            throw {
                code: "0400",
                message: "phone no is wrong"
            }
        }
    }

    const pushMessage = () => {
        const p = new Promise((resolve, reject) => {            
            if (!phoneNo) {
                reject({code: "0400", message: "phone no is wrong"})
            } else if(!receiveArray) {
                reject({code: "0401", message: "no incoming phone no"})
            } else if(!message) {
                reject({code: "0402", message: "no text message"})
            } else {
                for(var i in receiveArray)
                {
                    User.findOneByPhoneNo(receiveArray[i])
                    .then(push)
                }                
                resolve({})
            }
        })

        return p
    }

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success"
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    pushMessage()
    .then(respond)
    .catch(onError)
}

/*
    POST /api/service/message
*/

exports.pushMessage = (req, res) => {

    const getMyMessages = () => {
        return Message.findMessageByPhoneNo(req.phoneNo)
    }    

    const respond = (messages) => {

        if (messages) {
            var stream = res.push(json({
                message: messages
                }), 
                { 
                status: 200, // optional 
                method: 'GET', // optional
                request: { accept: '*/*' }, 
                response: { 'content-type': 'application/json' } 
            }); 
            
            stream.on('error', function() { }); 
            stream.end('document.write("hello from push stream!");'); 
            
            res.json({
                code: "0000",
                message: "success"
            })
        }
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    pushMessage()
    .then(respond)
    .catch(onError)
}

/*
    POST /api/service/conference
*/

exports.registeConference = (req, res) => {
    const {hostPhoneNo, subject, startDate, endDate, guestPhoneNo} = req.body

    var hostEmail = 'host'
    const newConferencePhoneNo = (host) => {
        const p = new Promise((resolve, reject) => {
            if(host)
                hostEmail = host.email

            if(hostPhoneNo)
            {
                var newConferencePhoneNo = Number("700" + hostPhoneNo)
                resolve({newConferencePhoneNo})
            } else {
                reject({
                    code: "0400",
                    message: "phone no is wrong"
                })
            } 
        })

        return p
    }

    const createConferenceItem = (param) => {
        const p = new Promise((resolve, reject) => {            
            if (!param.newConferencePhoneNo) {
                reject({code: "0403", message: "system failed to make new conference phone no"})
            } else if(!subject) {
                reject({code: "0404", message: "please input subject of conference"})
            } else if(!startDate) {
                reject({code: "0405", message: "please input start date"})
            } else if(!endDate) {
                reject({code: "0406", message: "please input end date"})   
            } else if(!guestPhoneNo || guestPhoneNo.length === 0) {
                reject({code: "0407", message: "please add guest phone no"})         
            } else {
                var conferencePhoneNo = param.newConferencePhoneNo
                var conference = {
                    conferencePhoneNo,
                    hostPhoneNo,
                    subject,
                    startDate,
                    endDate,
                    guestPhoneNo,
                }
                            
                resolve({conference})
            }
        })

        return p
    }

    const createConference = (item) => {
        return Conference.createConference(
            item.conference.conferencePhoneNo, 
            item.conference.hostPhoneNo,
            item.conference.subject,
            new Date(item.conference.startDate),
            new Date(item.conference.endDate),
            item.conference.guestPhoneNo
        )
    }

    const push = (calleePhoneNo) => {        
        if(calleePhoneNo) {
            var message = "Nofity conference call from " + hostPhoneNo + "\n"
                        + "Subject : " + subject + "\n"
                        + "Date : " + startDate + "~" + endDate
            return Message.push(
                hostPhoneNo, 
                hostEmail,
                calleePhoneNo.phoneNo, 
                calleePhoneNo.email,
                message, 
                misc.lenBytr(message)
            )
        } 
    }

    const pushMessage = (conference) => {
        const p = new Promise((resolve, reject) => {            
            if (conference) {
                User.findOneByPhoneNo(hostPhoneNo)
                    .then(push)

                for(var i in conference.guestPhoneNo)
                {
                    if (conference.guestPhoneNo[i].phoneNo !== hostPhoneNo) {
                        User.findOneByPhoneNo(conference.guestPhoneNo[i].phoneNo)
                        .then(push)
                    }
                }                
                resolve({conference})
            }
        })

        return p
    }    

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            conference: result
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })   
    }    

    User.findOneByPhoneNo(hostPhoneNo)
    .then(newConferencePhoneNo)
    .then(createConferenceItem)
    .then(createConference)
    .then(pushMessage)
    .then(respond)
    .catch(onError)
}

/*
    GET /api/service/conference
*/

exports.retrieveConference = (req, res) => {
    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            conference: result
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })   
    }
    
    //Conference.find({"hostPhoneNo": req.params.phoneNo})
    Conference.find({
         $or: [ { hostPhoneNo: req.params.phoneNo }, { "guestPhoneNo.phoneNo": req.params.phoneNo }] 
    })
    .sort({"startDate":-1})
    .limit(100)
    .then(respond)
    .catch(onError)
}

/*
    PUT /api/service/conference
*/

exports.updateConference = (req, res) => {

    const { _id, hostPhoneNo, subject, startDate, endDate, guestPhoneNo } = req.body

    var hostEmail = 'host'

    const push = (calleePhoneNo) => {        
        if(calleePhoneNo) {
            var message = "Nofity to change conference call from " + hostPhoneNo + "\n"
                        + "Subject : " + subject + "\n"
                        + "Date : " + startDate + "~" + endDate

            if (calleePhoneNo.phoneNo === hostPhoneNo)
                hostEmail = calleePhoneNo.email

            return Message.push(
                hostPhoneNo, 
                hostEmail,
                calleePhoneNo.phoneNo, 
                calleePhoneNo.email,
                message, 
                misc.lenBytr(message)
            )
        } 
    }

    const pushMessage = (conference) => {
        const p = new Promise((resolve, reject) => {            
            if (conference) {
                User.findOneByPhoneNo(hostPhoneNo)
                    .then(push)

                for(var i in conference.guestPhoneNo)
                {
                    if (hostPhoneNo !== conference.guestPhoneNo[i].phoneNo) {
                        User.findOneByPhoneNo(conference.guestPhoneNo[i].phoneNo)
                        .then(push)
                    }
                }                
                resolve({conference})
            }
        })

        return p
    }   

    const updateConference = (conference) => {       
        if (conference) {
            return conference.updateConference(subject, startDate, endDate, guestPhoneNo)
        } else {
            throw {
                code: "0408",
                message: "conference not exists"
            }
        }        
    }    

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            conference: result
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })   
    }

    Conference.findOne({"_id": _id, "hostPhoneNo": hostPhoneNo})
    .then(updateConference)
    .then(pushMessage)
    .then(respond)
    .catch(onError)
}

/*
    DELETE /api/service/conference
*/

exports.deleteConference = (req, res) => {
    const { _id, hostPhoneNo } = req.body
    var subject, startDate, endDate

    const deleteConference = () => {
        return Conference.deleteOne({"_id": _id, "hostPhoneNo": hostPhoneNo})
    }

    let hostEmail = 'DELETE'

    const push = (calleePhoneNo) => {        
        if(calleePhoneNo) {
            var message = "Nofity to cancel conference call from " + hostPhoneNo + "\n"
                        + "Subject : " + subject + "\n"
                        + "Date : " + startDate + "~" + endDate
            if (calleePhoneNo.phoneNo === hostPhoneNo)
                hostEmail = calleePhoneNo.email
            return Message.push(
                hostPhoneNo, 
                hostEmail,
                calleePhoneNo.phoneNo, 
                calleePhoneNo.email,
                message, 
                misc.lenBytr(message)
            )
        } 
    }

    const pushMessage = (conference) => {
        const p = new Promise((resolve, reject) => {      

            if (conference) {
                subject = conference.subject
                startDate = conference.startDate
                endDate = conference.endDate
                      
                User.findOneByPhoneNo(hostPhoneNo)
                    .then(push)
                    .catch(onError)

                for(var i in conference.guestPhoneNo)
                {
                    if (hostPhoneNo !== conference.guestPhoneNo[i].phoneNo) {
                        User.findOneByPhoneNo(conference.guestPhoneNo[i].phoneNo)
                        .then(push)
                        .catch(onError)
                    }
                }                
                resolve({conference})
            } else {
                reject({
                    code: "0408",
                    message: "conference not exists"
                })
            }

        })

        return p
    }   

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            conference: result
        })
    }

    const onError = (error) => {
        console.error(error.message)
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })   
    }

    Conference.findOne({"_id": _id, "hostPhoneNo": hostPhoneNo})
    .then(pushMessage)
    .then(deleteConference)
    .then(respond)
    .catch(onError)
}


/*
    POST /api/service/callhistory
*/

exports.saveCallHistory = (req, res) => {
    const {cid, callerPhoneNo, callerEmail, calleePhoneNo, calleeEmail, requestDate, startDate, endDate, duration, payYearMonth, payCompleted} = req.body

    const saveCallHistory = (callHistory) => {
        const p = new Promise((resolve, reject) => {            
            if (callHistory) {
                reject({code: "0400", message: "call history duplicated"})
            } else {
                    CallHistory.createCallHistory(cid, callerPhoneNo, callerEmail, calleePhoneNo, calleeEmail, requestDate, startDate, endDate, duration, payYearMonth, payCompleted)                                 
                resolve({})
            }
        })

        return p
    }

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success"
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    CallHistory.findOne({cid: cid})    
    .then(saveCallHistory)
    .then(respond)
    .catch(onError)
}

/*
    GET /api/service/callhistory
*/
exports.retrieveCallHistory = (req, res) => {
    

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            callHistory: result
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    CallHistory.find({
        $and : [ 
            { $or: [ { callerEmail: req.query.email }, { calleeEmail: req.query.email }] },
            { payYearMonth: req.query.payYearMonth}
        ]
    })
    .then(respond)
    .catch(onError)
}


/*
    GET /api/service/messageHistory
*/
exports.retrieveMessageHistory = (req, res) => {
    

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            messageHistory: result
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    Message.find({
        $and : [ 
            { $or: [ { callerEmail: req.query.email }, { calleeEmail: req.query.email }] },
            { payYearMonth: req.query.payYearMonth}
        ]
    })  
    .then(respond)
    .catch(onError)
}

/*
    GET /api/service/messageHistory
*/
exports.retrieveServiceHistory = (req, res) => {
    
    var tempHistory
    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            callHistory: result,
            messageHistory: tempHistory
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    Message.find({
        $and : [ 
            { $or: [ { callerEmail: req.query.email }, { calleeEmail: req.query.email }] },
            { payYearMonth: req.query.payYearMonth}
        ]
    })
    .then((messageHistory) => {
        tempHistory = messageHistory

        return CallHistory.find({
            $and : [ 
                { $or: [ { callerEmail: req.query.email }, { calleeEmail: req.query.email }] },
                { payYearMonth: req.query.payYearMonth}
            ]
        })
    })
    .then(respond)
    .catch(onError)
}


/*
    GET /api/service/dashboard
*/
exports.retrieveDashboard = (req, res) => {
    
    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            messageSummary: result[0],         
            callSummary: result[1],
            personSummary: result[2],
            
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    if (req.query.email) {
        var messageSummary = Message.aggregate([
            {$match: {$and : [ 
                { callerEmail: req.query.email },
                { payYearMonth: req.query.payYearMonth}
            ]}},
            {$project: {yearMonthDay: { $dateToString: { format: "%Y-%m-%d", date: "$requestDate" } },  "chargePerMessage":1}},
            {$group: {_id:"$yearMonthDay", totalOfCharge: {$sum: "$chargePerMessage"}, count: {$sum: 1}}},
            {$project: {"yearMonthDay":"$_id", "totalOfCharge":1, "count":1}},
            {$sort: {"yearMonthDay":1}}
        ]).exec()

        var callSummary = CallHistory.aggregate([
                {$match: {$and : [ 
                    { callerEmail: req.query.email },
                    { payYearMonth: req.query.payYearMonth}
                ]}},
                {$project: {yearMonthDay: { $dateToString: { format: "%Y-%m-%d", date: "$requestDate" } }, "totalOfCharge": {$multiply:["$chargePerMinute", "$duration"]}, "duration": 1, count: {$sum: 1}}},
                {$group: {_id:"$yearMonthDay", totalOfCharge: {$sum: "$totalOfCharge"}, totalOfDuration: {$sum: "$duration"}, count: {$sum: 1}}},
                {$project: {"yearMonthDay":"$_id", "totalOfCharge":1, "totalOfDuration": 1, "totalOfDuration": 1, "count":1}},
                {$sort: {"yearMonthDay":1}}
            ]).exec()
        
        var personSummary = User.count({}).exec()

        var allDone = Promise.all([messageSummary, callSummary, personSummary])

        allDone
        .then(respond)
        .catch(onError)
    } else {
        var messageSummary = Message.aggregate([
            {$match: {payYearMonth: req.query.payYearMonth}},
            {$project: {yearMonthDay: { $dateToString: { format: "%Y-%m-%d", date: "$requestDate" } },  "chargePerMessage":1}},
            {$group: {_id:"$yearMonthDay", totalOfCharge: {$sum: "$chargePerMessage"}, count: {$sum: 1}}},
            {$project: {"yearMonthDay":"$_id", "totalOfCharge":1, "count":1}},
            {$sort: {"yearMonthDay":1}}
        ]).exec()

        var callSummary = CallHistory.aggregate([
            {$match: {payYearMonth: req.query.payYearMonth}},
            {$project: {yearMonthDay: { $dateToString: { format: "%Y-%m-%d", date: "$requestDate" } }, "totalOfCharge": {$multiply:["$chargePerMinute", "$duration"]}, "duration": 1}},
            {$group: {_id:"$yearMonthDay", totalOfCharge: {$sum: "$totalOfCharge"}, totalOfDuration: {$sum: "$duration"}, count: {$sum: 1}}},
            {$project: {"yearMonthDay":"$_id", "totalOfCharge":1, "totalOfDuration": 1, "totalOfDuration": 1, "count":1}},
            {$sort: {"yearMonthDay":1}}
        ]).exec()

        var personSummary = User.count({}).exec()

        var allDone = Promise.all([messageSummary, callSummary, personSummary])

        allDone
        .then(respond)
        .catch(onError)      
    }
}