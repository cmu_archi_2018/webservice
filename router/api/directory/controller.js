const User = require('../../../models/user')
const convertIPv4 = require('../../../utils/ip-address')
const Directory = require('../../../models/directory')
const Message = require('../../../models/message')

/*
    POST /api/directory/status
*/

exports.updateStatus = (req, res) => {
    const {ipAddress} = req.body

    const updateStatus = (directory) => {
        const p = new Promise((resolve, reject) => {
            if (!ipAddress) {
                reject({code: "0300", message: "wrong ipaddress"})
            } else if (!directory) {                
                Directory.createStatus(req.params.phoneNo, ipAddress)
                resolve({directory})
            } else {
                directory.updateStatus(ipAddress)
                resolve({directory})
            }
        })

        return p
    }

    const getMyMessages = (directory) => {
        return Message.findMessageByPhoneNo(directory.directory.phoneNo)
    }

    const respond = (messages) => {
        const p = new Promise((resolve) => {
            res.json({
                code: "0000",
                message: "success",
                textMessages: messages
            })

            resolve({messages})
        })

        return p        
    }

    const popMessage = (messages) => {
        return Message.pop(messages.messages.length > 0 ? messages.messages[0].calleePhoneNo : "undefined")
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })   
    }    

    Directory.findOneByPhoneNo(req.params.phoneNo)
    .then(updateStatus)
    .then(getMyMessages)
    .then(respond)
    .then(popMessage)
    .catch(onError)
}

/*
    GET /api/directory/status
*/

exports.getDirectory = (req, res) => {
    
    const getStatus = (directory) => {
        const p = new Promise((resolve, reject) => {
            if (directory) {
                res.json({
                    code: "0000",
                    message: 'success',
                    directory: directory
                })
                resolve({})
            } else {
                reject({code: "0301", message: "directory info not exists"})
            }
        })

        return p
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })   
    }    

    Directory.findOneByPhoneNo(req.params.phoneNo)
    .then(getStatus)
    .catch(onError)
}