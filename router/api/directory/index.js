const router = require('express').Router()
const controller = require('./controller')

router.post('/status/:phoneNo', controller.updateStatus)
router.get('/status/:phoneNo', controller.getDirectory)

module.exports = router