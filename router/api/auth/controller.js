const User = require('../../../models/user')
const Email = require('../../../utils/email')
const Misc = require('../../../utils/misc')

/*
    POST /api/auth/register
    {
        email,
        password, 
        role, 
        address, 
        payment
    }
*/

exports.register = (req, res) => {
    const { email, password, role, address, payment } = req.body

    let newUser = null

    const getNewPhoneNo = (user) => {
        const p = new Promise((resolve, reject) => {
            if(user) {            
                reject({
                    code: "0001",
                    message: "email already exists"
                })
            } else {            
                resolve(User.getNewPhoneNo())
            }
        })

        return p
    }

    const create = (newPhoneNo) => {
        return User.create(email, newPhoneNo, password, role, address, payment)
    }

    const count = (user) => {
        newUser = user
        return User.count({}).exec()
    }

    const assign = (count) => {
        // first user will be assigned role of admin.
        if(count === 1) {
            return newUser.assignRole('admin')
        } else {            
            return Promise.resolve(newUser)
        }
    }

    const respond = (newUser) => {
        newUser.password = ""
        res.json({
            code: "0000",
            message: 'registed successfully',
            user: newUser
        })
    }

    const onError = (error) => {
        // res.status(409).json({
        //     code: error.code ? error.code : "0099",
        //     message: error.message
        // })
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }

    User.findOneByEmail(email)
    .then(getNewPhoneNo)
    .then(create)
    .then(count)
    .then(assign)
    .then(respond)
    .catch(onError)
}

const jwt = require('jsonwebtoken')

/*
    POST /api/auth/login
    {
        email,
        password
    }
*/

exports.login = (req, res) => {
    const { email, password } = req.body
    const secret = req.app.get('jwt-secret')

    const check = (user) => {
        if(!user) {
            throw {
                code: "0200",
                message: "login failed"
            }
        } else if (user.role != "admin" && user.role != "enable" && user.role != "general") {
            throw {
                code: "0200",
                message: "Your account has been stopped."
            }
        } else {
            if (user.verify(password, user.salt?user.salt:secret)) {
                const p = new Promise((resolve, reject) => {
                    jwt.sign(
                        {
                            _id: user._idm,
                            email: user.email,
                            phoneNo: user.phoneNo,
                            role: user.role
                        },
                        secret,
                        {
                            expiresIn: '7d',
                            issuer: 'team4.cmu.edu',
                            subject: 'userInfo'                        
                        }, (err, token) => {
                            if (err) reject(err)
                            resolve({user, token})
                        }
                    )
                })

                return p
            } else {
                throw {
                    code: "0201",
                    message: "wrong password"
                }
            }            
        }
    }

    const respond = (result) => {
        result.user.password = ""
        result.user.salt=""
        if (result.user.payment || result.user.payment.length > 0)
            result.user.payment[0].cardNo = ""
        res.json({
            code: "0000",
            message: 'logged in successfully',
            token: result.token,
            user: result.user
        })
    }

    const onError = (error) => {
        // res.status(403).json({
        //     code: error.code ? error.code : "0099",
        //     message: error.message
        // })
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })       
    }

    User.findOneByEmail(email)
    .then(check)
    .then(respond)
    .catch(onError)
}

/*
    GET /api/auth/check
*/

exports.check = (req, res) => {
    res.json({
            code: "0000",
            message: "success",            
            info: req.decoded
    })
}

/*
    POST /api/auth/recover
*/

exports.requestNewPassword = (req, res) => {
    const { email } = req.body
    const secret = req.app.get('jwt-secret-reset-password')

    const check = (user) => {
        if(!user) {
            throw {
                code: "0203",
                message: "email not exists"
            }
        } else {            
            const p = new Promise((resolve, reject) => {
                
                const tempPassword = Misc.tempPassword()
                jwt.sign(
                    {
                        _id: user._idm,
                        email: user.email,
                        phoneNo: user.phoneNo,
                        role: user.role
                    },
                    secret,
                    {
                        expiresIn: '5m',  // within 5 minutes
                        issuer: 'cmd.edu',
                        subject: tempPassword                        
                    }, (err, token) => {
                        if (err) reject(err)                        
                        resolve({user, tempPassword, token})
                    }
                )
            })

            return p                      
        }
    }

    const sendResetMail = (resetToken) => {         
        const p = new Promise((resolve, reject) => {
            if(Email.resetPassword(resetToken.user.email, resetToken.tempPassword, resetToken.token)) {
                resolve({})
            }
            else {
                reject({
                    code: "0207",
                    message: "system fail to send email for reset password"
                })
            }
        })

        return p                      
    }

    const respond = () => {
        res.json({
            code: "0000",
            message: 'reset password successfully'
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })     
    }

    User.findOneByEmail(email)
    .then(check)
    .then(sendResetMail)
    .then(respond)
    .catch(onError)
}


/*
    GET /api/auth/recover
*/

exports.resetPassword = (req, res) => {
    
    const check = (user) => {
        const p = new Promise((resolve, reject) => {
            jwt.verify(req.query.token, req.app.get('jwt-secret-reset-password'), (err, decoded) => {
                if(err) {
                    reject(err)
                } else {
                    resolve({user, decoded})
                }
            })
        })

        return p
    }

    const resetPassword = (userInfo) => {
        var salt = Misc.tempPassword(16)
        return userInfo.user.resetPassword(userInfo.decoded.sub, salt)
    }

    const respond = (user) => {
        res.json({
            code: "0000",
            message: 'reset password successfully'
        })
    }

    const onError = (error) => {     
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })        
    }
    
    User.findOneByEmail(req.query.email)
    .then(check)
    .then(resetPassword)
    .then(respond)
    .catch(onError)
}


/*
    POST /api/auth/logout
    {
        email
    }
*/

exports.logout = (req, res) => {
    const { email } = req.body

    const check = (user) => {
        if(!user) {
            throw {
                code: "0200",
                message: "logout failed"
            }
        } else {
            console.log("user logout ===> " + user.email)                        
        }
    }

    const respond = (result) => {
        res.json({
            code: "0000",
            message: 'logout successfully'
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })       
    }

    User.findOneByEmail(email)
    .then(check)
    .then(respond)
    .catch(onError)
}

