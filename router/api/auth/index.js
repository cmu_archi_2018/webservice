const router = require('express').Router()
const controller = require('./controller')
const authMiddleware = require('../../../middlewares/auth')

router.post('/register', controller.register)
router.post('/login', controller.login)
router.post('/logout', controller.logout)
router.use('/check', authMiddleware)
router.get('/check', controller.check)
router.post('/recover', controller.requestNewPassword)
router.get('/recover/', controller.resetPassword)

module.exports = router