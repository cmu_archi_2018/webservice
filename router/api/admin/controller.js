const ChargePolicy = require('../../../models/chargepolicy')
const moment    = require('moment');

/*
    POST /api/admin/chargepolicy
*/

exports.saveChargePolicy = (req, res) => {
    const {chargeofMinute, chargeofMessage, applyDate} = req.body

    const saveChargePolicy = (chargePolicy) => {
        const p = new Promise((resolve, reject) => {  
            if (chargePolicy) {
                reject({code: "0400", message: "Duplicate dates are not allowed."})
            } else if (!chargeofMinute || !chargeofMessage) {
                reject({code: "0400", message: "MUST be inputed value of charge!!!"})
            } else if (applyDate < Date.now()) {
                reject({code: "0400", message: "The effective date must be entered after today."})
            } else {
                ChargePolicy.createChargePolicy(chargeofMinute, chargeofMessage, applyDate)                                 
                resolve({})
            }
        })

        return p
    }

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success"
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    ChargePolicy.findOne({applyDate: applyDate})    
    .then(saveChargePolicy)
    .then(respond)
    .catch(onError)
}

/*
    GET /api/admin/chargepolicy
*/
exports.retrieveChargePolicy = (req, res) => {

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            chargePolicy: result
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    ChargePolicy.find({})
    .sort({applyDate: -1})
    .then(respond)
    .catch(onError)
}


/*
    PATCH /api/admin/chargepolicy
*/
exports.updateChargePolicy = (req, res) => {
    const {createDate, chargeofMinute, chargeofMessage, isApply, isCurrent, applyDate} = req.body

    const updateChargePolicy = (chargePolicy) => {
        const p = new Promise((resolve, reject) => {
            if (!chargePolicy) {
                reject({code: "0400", message: "No date to update"})
            } else if (!chargeofMinute || !chargeofMessage) {
                reject({code: "0400", message: "MUST be inputed value of charge!!!"})
            } else if (applyDate < Date.now()) {
                reject({code: "0400", message: "The effective date must be entered after today."})
            } else {
                chargePolicy.updateChargePolicy(chargeofMinute, chargeofMessage, applyDate, isApply, isCurrent)               
                resolve({})                
            }
        })

        return p
    }    
    
    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            callHistory: result
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    ChargePolicy.findOne({ createDate: createDate })
    .then(updateChargePolicy)    
    .then(respond)
    .catch(onError)
}


/*
    POST /api/admin/applycharge
*/
exports.applyCharge = (req, res) => {

    const releaseCurrentPolicy = (curretPolicy) => {
        return curretPolicy.updateCurrentPolicy("N")
    }
    
    const changePolicy = (chargePolicy) => {
        const p = new Promise((resolve, reject) => {
            if (chargePolicy && chargePolicy.length > 0) {
                ChargePolicy.findOne({isCurrent:"Y"})
                .then(releaseCurrentPolicy)
                .catch(onError)

                chargePolicy[0].updateCurrentPolicy("Y")
            }

            resolve({}) 
        })
        
        return p
    }

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success"
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    //var today = Date.now()
    var today = moment().format("YYYY-MM-DD 00:00:00")

    ChargePolicy.find({
        $and : [ 
            { isApply:"Y" },
            //{ applyDate: { $gte:today } }
            { applyDate: today }
        ]
    })
    .sort({applyDate: 1})
    .then(changePolicy)
    .then(respond)
    .catch(onError)
}