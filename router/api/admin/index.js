const router = require('express').Router()
const controller = require('./controller')
const authMiddleware = require('../../../middlewares/auth')

router.post('/chargepolicy', controller.saveChargePolicy)
router.get('/chargepolicy', controller.retrieveChargePolicy)
router.patch('/chargepolicy', controller.updateChargePolicy)
router.post('/applycharge', controller.applyCharge)

module.exports = router