const router = require('express').Router()
const controller = require('./controller')

router.get('/list', controller.list)
router.post('/assignrole', controller.assignAdmin)
router.post('/password/:email', controller.changePassword)
router.post('/profile/:email', controller.updateProfile)

module.exports = router