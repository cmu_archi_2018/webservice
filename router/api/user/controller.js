const User = require('../../../models/user')

/*
    GET /api/user/list
*/

exports.list = (req, res) => {
    if(!req.decoded.role) { //} || req.decoded.role !== "admin") {
        return res.status(403).json({
            code: "0202",
            message: 'you are not an admin'
        })
    }


    User.find({$or: [ { role: "admin" }, { role: "enable" }, { role: "disable"}, { role: "general"}]},
              {email:1, role:1, address:1, phoneNo:1, payment:1, createDate:1, updateDate:1})
    .then(
        users=>{
            res.json({
                code: "0000",
                message: 'success',
                users: users
            })
        }
    )
}

/*
    POST /api/user/assign-admin/:email
*/

exports.assignAdmin = (req, res) => {

    const { email, role } = req.body

    // if(!req.decoded.role || req.decoded.role !== "admin") {
    //     return res.status(403).json({
    //         code: "0202",
    //         message: 'you are not a admin'
    //     })
    // }

    const check = (user) => {
        const p = new Promise((resolve, reject) => {
            if (!user) {
                //reject(new Error('email not exists'))
                reject({code: "0203", message: "email not exists"})
            } else {
                    user.assignRole(role)
                    resolve({})
                }
            })

        return p
    }

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success"
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })    
    }

    User.findOneByEmail(email)
    .then(check)
    .then(respond)
    .catch(onError)
}

/*
    POST /api/user/password/:email
*/

exports.changePassword = (req, res) => {
    const { oldPassword, newPassword } = req.body

    const changePassword = (user) => {
        const p = new Promise((resolve, reject) => {
            if (!user) {
                reject({code: "0203", message: "email not exists"})
            } else {
                    if(!user.changePassword(oldPassword, newPassword, user.salt)) {
                        reject({code: "0201", message: "wrong password"})
                    } else {
                        resolve({})
                    }
                }
            })

        return p
    }

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success"
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })    
    }

    User.findOneByEmail(req.params.email)
    .then(changePassword)
    .then(respond)
    .catch(onError)    
}

/*
    POST /api/user/profile/:email
*/

exports.updateProfile = (req, res) => {
    const { address, payment } = req.body

    const updateProfile = (user) => {
        const p = new Promise((resolve, reject) => {
            if (!user) {
                reject({code: "0203", message: "email not exists"})
            } else {
                    if(!user.updateProfile(address, payment)) {
                        reject({code: "0102", message: "wrong profile"})
                    } else {
                        resolve({})
                    }
                }
            })

        return p
    }

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success"
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })    
    }

    User.findOneByEmail(req.params.email)
    .then(updateProfile)
    .then(respond)
    .catch(onError)    
}