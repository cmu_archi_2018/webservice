const router = require('express').Router()
const controller = require('./controller')

router.post('/bill', controller.makeBill)
router.get('/bill', controller.retrieveBill)

module.exports = router