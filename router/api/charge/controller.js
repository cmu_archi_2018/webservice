const User = require('../../../models/user')
const Message = require('../../../models/message')
const CallHistory = require('../../../models/callhistory')
const Bill = require('../../../models/bill')
const misc = require('../../../utils/misc')



/*
    POST /api/charge/bill
*/

exports.makeBill = (req, res) => {    

    const { payYearMonth } = req.body

    const makeEmptyBill = (user) => {
        return Bill.createBill(
            user.email, 
            user.phoneNo,
            payYearMonth, 
            "N", 
            user.payment[0].cardNo, 
            Date.now(), 
            "process"
        )
    }

    const getCallSummary = (bill) => {
        return CallHistory.getCallSummary(bill.email, bill.payYearMonth)
    }

    const getMessageSummary = (bill) => {
        return Message.getMessageSummary(bill.email, bill.payYearMonth)
    }

    const makeChargeBill = (bill) => {        
        Bill.findOne({email: bill.email, payYearMonth: bill.payYearMonth})
        .then(getCallSummary)
        .then(makeChargeOfCall)
        .catch(onError)

        Bill.findOne({email: bill.email, payYearMonth: bill.payYearMonth})
        .then(getMessageSummary)
        .then(makeChargeOfMessage)
        .catch(onError)
    }

    const makeChargeOfCall = (callSummary) => {    
        let duration, chargeOfCall
        if (callSummary && callSummary.length > 0) {
            duration = callSummary[0].totalOfDuration
            chargeOfCall = callSummary[0].totalOfCharge
            return  Bill.updatBillofCall(callSummary[0].callerEmail, callSummary[0].payYearMonth, duration, chargeOfCall)
        }

        return Promise.resolve({})
    }

    const makeChargeOfMessage = (messageSummary) => {       
        let messageQty, chargeOfMessages
        if (messageSummary && messageSummary.length > 0) {
            messageQty = messageSummary[0].count
            chargeOfMessages = messageSummary[0].totalOfCharge
            return Bill.updatBillofMessage(messageSummary[0].callerEmail, messageSummary[0].payYearMonth, messageQty, chargeOfMessages)
        }

        return Promise.resolve({})
    }

    const createBill = (bill) => {
        Bill.createBill(user.email, 
            payYearMonth, 
            "Y", 
            duration, 
            messageQty, 
            chargeOfCall, 
            chargeOfMessages, 
            chargeOfCall+chargeOfMessages,
            user.payment.cardNo, 
            Date.now(), 
            "success")

        var message = "Charged mobile pee for this month. \n"
            + "Charge of call : $" + chargeOfCall + "\n"
            + "Charge of messages : $" + chargeOfMessages + "\n"
            + "Total : $" + chargeOfCall+chargeOfMessages

        Message.push(
            "000", 
            userList[i].phoneNo, 
            message, 
            misc.lenBytr(message)
        )
    }    

    const makeBill = (userList) => {
        const p = new Promise((resolve, reject) => {            
            if (!userList) {
                reject({code: "0400", message: "user list is wrong"})
            } else {
                

                for(var i in userList) {
                    user = userList[i]
                    
                     makeEmptyBill(user)
                    .then(makeChargeBill)
                    .catch(onError)
                }

                resolve({})
            }
        })

        return p
    }
    
    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success"
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    Bill.deleteMany({payYearMonth: payYearMonth})
    .then(() => User.find({}))
    .then(makeBill)
    .then(respond)
    .catch(onError)
}

/*
    GET /api/charge/bill
*/
exports.retrieveBill = (req, res) => {
    

    const respond = (result) => {
        res.json({
            code: "0000",
            message: "success",
            bills: result
        })
    }

    const onError = (error) => {
        res.status(200).json({
            code: error.code ? error.code : "0099",
            message: error.message
        })
    }    

    if (req.params.payYearMonth) {
        Bill.find({email: req.query.email, payYearMonth: req.query.payYearMonth})
        .then(respond)
        .catch(onError)
    } else {
        Bill.find({email: req.query.email})
        .then(respond)
        .catch(onError)
    }        
}