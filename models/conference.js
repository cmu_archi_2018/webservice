const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ConferenceSchema = new Schema({
    conferencePhoneNo: {type: String, required: true},
    hostPhoneNo: {type: String, required: true},
    subject: {type: String, required: true},    
    startDate: {type: Date, required: true},
    endDate: {type: Date, required: true},
    guestPhoneNo: [
        {
            phoneNo: {type: String}
        }
    ],
    requestDate: {type: Date, default: Date.now()}
})

ConferenceSchema.index({ startDate: -1, endDate: -1, conferencePhoneNo: 1})
ConferenceSchema.index({ hostPhoneNo: 1, startDate: -1, endDate: -1 })

ConferenceSchema.statics.createConference = function(conferencePhoneNo, hostPhoneNo, subject, startDate, endDate, guestPhoneNo) {
    const conferenceCall = new this({
        conferencePhoneNo,
        hostPhoneNo,
        subject,
        startDate,
        endDate,
        guestPhoneNo
    })
    
    // return the Promise
    return conferenceCall.save() 
}

ConferenceSchema.methods.updateConference = function(subject, startDate, endDate, guestPhoneNo)
{
    this.subject = subject
    this.startDate = startDate
    this.endDate = endDate    
    this.guestPhoneNo = guestPhoneNo

    return this.save() 
}

module.exports = mongoose.model('Conference', ConferenceSchema)