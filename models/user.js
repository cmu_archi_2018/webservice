const mongoose = require('mongoose')
const Schema = mongoose.Schema
const crypto = require('../utils/crypto')
const { tempPassword } = require('../utils/misc')

const Payment = new Schema({
    cardNo: {type: String, trim: true},
    expirationDate: {type: String, trim: true},
    verificationCode: {type: String, trim: true}
})

const User = new Schema({
    email: {type: String, required: true, unique: true, trim: true},
    password: {type: String, required: true, trim: true},
    role: {type: String, required: true, default: 'enable'},
    address: {type: String},
    phoneNo: {type: String, required: true, unique: true, trim: true},
    payment: [Payment],
    // payment: [
    //     {
    //         cardNo: {type: String, trim: true},
    //         expirationDate: {type: String, trim: true},
    //         verificationCode: {type: String, trim: true}
    //     }
    // ],
    createDate: {type: Date, default: Date.now()},
    updateDate: {type: Date, default: Date.now()},
    salt: {type: String}
})

User.index({ phoneNo: -1})

User.statics.getNewPhoneNo = async function() {
    var lastphoneNo = await this.aggregate([
        { 
            "$project": 
            {
                "phoneNo": "$phoneNo"
            }
        },
        {"$sort": {"phoneNo":-1}},
        {"$limit": 1},
    ])

    var newPhoneNo = 100

    if(lastphoneNo && lastphoneNo.length !== 0) {
        newPhoneNo = Number(lastphoneNo[0].phoneNo) + 1
    }

    return newPhoneNo.toString()
}

// create new User document
User.statics.create = function(email, phoneNo, password, role, address, payment) {

    var salt = tempPassword(16)
    const encrypted = crypto(password, salt)

    const user = new this({
        email,
        password: encrypted.password,
        role,
        address,        
        phoneNo,
        payment,
        salt
    })
    
    // return the Promise
    return user.save()  
}

// find one user by using username
User.statics.findOneByEmail = function(email) {
    return this.findOne({
        email: email
    }).exec()
}

User.statics.findOneByPhoneNo = function(phoneNo) {
    return this.findOne({
        phoneNo: phoneNo
    }).exec()
}

// verify the password of the User documment
User.methods.verify = function(password, salt) {
    const encrypted = crypto(password, salt)

    return this.password === encrypted.password
}

User.methods.assignRole = function(role) {
    this.role = role
    this.updateDate = Date.now()
    return this.save()    
}

User.methods.changePassword = function(oldPasword, newPassword, salt) {
    if(this.verify(oldPasword, salt))
    {
        var salt = tempPassword(16)
        var encypted = crypto(newPassword, salt)
        this.password = encypted.password
        this.updateDate = Date.now()
        this.salt = encypted.salt
        return this.save()
    } else {
        return false
    }
}

User.methods.resetPassword = function(password, salt) {
    var crypted = crypto(password, salt)
    this.password = crypted.password
    this.updateDate = Date.now()
    this.salt = crypted.salt
    return this.save()
}

User.methods.updateProfile = function(address, payment) {
    this.address = address
    this.payment = payment
    this.updateDate = Date.now()
    return this.save()    
}

module.exports = mongoose.model('User', User)