const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Directory = new Schema({
    phoneNo: {type: String, required: true, unique: true},
    ipAddress: {type: String, required: true},
    updateDate: {type: Date, default: Date.now()},
    status: {type: String}                            // 사용자 상태(offline, stand-by, calling, so-on)
    // If we have more time, may be next time!!
    // history: [
    //     {
    //         updateDate: {type: String},
    //         ipAddress: {type: String}                        
    //     }
    // ]
})

// find one user by using username
Directory.statics.findOneByPhoneNo = function(phoneNo) {
    return this.findOne({
        phoneNo: phoneNo
    }).exec()
}

Directory.methods.updateStatus = function(ipAddress) {
    this.ipAddress = ipAddress    
    this.updateDate = Date.now()
    return this.save()
}

Directory.statics.createStatus = function(phoneNo, ipAddress) {
    const directory = new this({
        phoneNo,
        ipAddress
    })
    
    // return the Promise
    return directory.save() 
}

module.exports = mongoose.model('Directory', Directory)