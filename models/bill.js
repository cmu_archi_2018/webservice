const mongoose = require('mongoose')
const Schema = mongoose.Schema

const BillSchema = new Schema({    
    email: {type: String, required: true, trim: true},
    phoneNo: {type: String, required: true},
    payYearMonth: {type: String},                  // 요금 정산 월
    payCompleted: {type: String, default: 'N'},  // 요금 정산 처리 여부
    duration: {type: Number, default: 0},
    messageQty: {type: Number, default: 0},
    chargeOfCall: {type: Number, default: 0},
    chargeOfMessages: {type: Number, default: 0},
    chargeOfTotal: {type: Number, default: 0},
    cardNo: {type: String, trim: true},
    payDate: {type: Date},
    payResult: {type: String}
})

BillSchema.index({ email: -1, payYearMonth: -1 })
BillSchema.index({ phoneNo: -1, payYearMonth: -1 })
BillSchema.index({ payYearMonth: 1 })
BillSchema.index({ payCompleted: 1 })


BillSchema.statics.createBill = function(email,
                                        phoneNo,
                                        payYearMonth,
                                        payCompleted,
                                        cardNo,
                                        payDate,
                                        payResult) {
    const bill = new this({
        email,
        phoneNo,
        payYearMonth,
        payCompleted,
        cardNo,
        payDate,
        payResult
    })

    // return this.updateOne({"email": email, "payYearMonth": payYearMonth}, 
    // {$set: {"email": email, "phoneNo": phoneNo}, "payYearMonth": payYearMonth, "payCompleted": payCompleted, "cardNo": cardNo, "payDate": payDate, "payResult": payResult},
    // {upsert: true})
    
    // return the Promise
    return bill.save()
}

BillSchema.statics.updatBillofCall = function(email, payYearMonth, duration, chargeOfCall) {
    return this.updateMany({"email": email, "payYearMonth": payYearMonth}, 
    {$set: {"duration": duration, "chargeOfCall": chargeOfCall}})
}

BillSchema.statics.updatBillofMessage = function(email, payYearMonth, messageQty, chargeOfMessages) {
    return this.updateMany({"email": email, "payYearMonth": payYearMonth}, 
    {$set: {"messageQty": messageQty, "chargeOfMessages": chargeOfMessages}})
}

module.exports = mongoose.model('Bill', BillSchema)