const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ChargePolicySchema = new Schema({    
    chargeofMinute: {type: Number, required: true},
    chargeofMessage: {type: Number, required: true},
    createDate: {type: Date, default: Date.now()},
    updateDate: {type: Date, default: Date.now()},
    applyDate: {type: Date, required: true},
    isApply: {type: String, default: 'Y'},
    isCurrent: {type: String, default: 'N'},
})

ChargePolicySchema.index({ isApply: 1, applyDate: -1 })
ChargePolicySchema.index({ isCurrent: 1 })

ChargePolicySchema.statics.createChargePolicy = function(chargeofMinute, chargeofMessage, applyDate) {
    const callHistory = new this({
        chargeofMinute,
        chargeofMessage,
        applyDate
    })
    
    // return the Promise
    return callHistory.save() 
}

ChargePolicySchema.methods.updateChargePolicy = function(chargeofMinute, chargeofMessage, applyDate, isApply, isCurrent) {
    this.chargeofMinute = chargeofMinute
    this.chargeofMessage = chargeofMessage
    this.applyDate = applyDate
    this.isApply = isApply
    this.isCurrent = isCurrent
    
    // return the Promise
    return this.save() 
}


ChargePolicySchema.methods.completePayment = function(isApply) {
    this.isApply = isApply
    this.updateDate = Date.now()

    return this.save()
}

ChargePolicySchema.methods.updateCurrentPolicy = function(isCurrent) {
    this.isCurrent = isCurrent
    this.updateDate = Date.now()

    return this.save()
}

module.exports = mongoose.model('ChargePolicy', ChargePolicySchema)