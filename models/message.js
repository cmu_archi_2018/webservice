const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MessageSchema = new Schema({
    callerPhoneNo: {type: String, required: true},
    callerEmail: {type: String, required: true},   // Caller Email address
    calleePhoneNo: {type: String, required: true},
    calleeEmail: {type: String, required: true},   // Caller Email address
    requestDate: {type: Date, default: Date.now()},
    isComplete: {type: Boolean, required: true, default: false},
    sendDate: {type: Date},
    textMessage: {type: String, required: true},
    textLength: {type: Number, requried: true},
    chargePerMessage: {type: Number},
    payYearMonth: {type: String, default: () => {
        var date = new Date();
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth()+1).toString();    
      
        var mmChars = mm.split('');
      
        return yyyy + (mmChars[1]?mm:"0"+mmChars[0]);
      } },                  // 요금 정산 월 (201801, 201807 .....)
    payCompleted: {type: String, default: 'N'},  // 요금 정산 처리 여부
    payDate: {type: Date}
})

MessageSchema.index({ calleePhoneNo: 1, isComplete: 1, requestDate: 1 })
MessageSchema.index({ callerPhoneNo: 1, requestDate: 1 })

// create new User document
MessageSchema.statics.push = function(callerPhoneNo, callerEmail, calleePhoneNo, calleeEmail, textMessage, textLength) {
    const message = new this({
        callerPhoneNo,
        callerEmail,
        calleePhoneNo,
        calleeEmail,
        textMessage,
        textLength
    })
    
    // return the Promise
    return message.save()  
}

MessageSchema.statics.findMessageByPhoneNo = function(phoneNo) {
    return this.find({
        calleePhoneNo: phoneNo,
        isComplete: false
    })
    .sort({requestDate: 1})
    .exec()
}

MessageSchema.statics.pop = function(phoneNo) {
    return this.updateMany(
        {"calleePhoneNo": phoneNo, "isComplete": false},
        {"isComplete": true, "sendDate": Date.now()},
        {upsert: false}               
    ).exec()
}

MessageSchema.statics.getMessageSummary = function(email, payYearMonth) {   
    if (email) {
        return this.aggregate([
            {$match: {"payYearMonth": payYearMonth, "callerEmail": email}}, {$group: {_id:"$callerEmail", payYearMonth: {$max: "$payYearMonth"}, total: {$sum: "$chargePerMessage"}, count: {$sum: 1}}},
            {"$project": {"callerEmail":"$_id", "payYearMonth":"$payYearMonth", "totalOfCharge":"$total", "count": "$count"}},
            {"$sort": {"callerEmail":-1}}
        ]).exec()
    } else {
        return this.aggregate([
            {$match: {"payYearMonth": payYearMonth}}, {$group: {_id:"$callerEmail", payYearMonth: {$max: "$payYearMonth"}, total: {$sum: "$chargePerMessage"}, count: {$sum: 1}}},
            {"$project": {"callerEmail":"$_id", "payYearMonth":"$payYearMonth", "totalOfCharge":"$total", "count": "$count"}},
            {"$sort": {"callerEmail":-1}}
        ]).exec()
    }
}

MessageSchema.methods.compltePayment = function() {
    this.payCompleted = 'Y'
    this.paymentDate = Date.now()

    return this.save()
}

 module.exports = mongoose.model('Message', MessageSchema)