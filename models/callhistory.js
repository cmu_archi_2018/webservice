const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CallHistorySchema = new Schema({
    cid: {type: String, required: true, unique: true},
    callerPhoneNo: {type: String, required: true}, // Caller Phone No
    callerEmail: {type: String, required: true},   // Caller Email address
    calleePhoneNo: {type: String, required: true}, // Callee Phone No
    calleeEmail: {type: String, required: true},   // Callee Email address
    requestDate: {type: Date, required: true},     // 전화 요청 일시
    startDate: {type: Date, required: true},       // 전화 통화 시작 일시
    endDate: {type: Date},                         // 전화 통화 종료 일시
    duration: {type: Number, required: true},      // 분 단위 통화 시간
    chargePerMinute: {type: Number},
    chargeOfTotal: {type: Number},
    payYearMonth: {type: String, default: () => {
        var date = new Date();
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth()+1).toString();    
      
        var mmChars = mm.split('');
      
        return yyyy + (mmChars[1]?mm:"0"+mmChars[0]);
      }},                  // 요금 정산 월 (201801, 201807 .....)
    payCompleted: {type: String, default: 'N'},    // 요금 정산 처리 여부
    payDate: {type: Date}
})
CallHistorySchema.index({ cid: 1 })
CallHistorySchema.index({ callerPhoneNo: -1, startDate: -1 })
CallHistorySchema.index({ callerEmail: -1, startDate: -1 })
CallHistorySchema.index({ calleePhoneNo: -1, startDate: -1 })
CallHistorySchema.index({ calleeEmail: -1, startDate: -1 })
CallHistorySchema.index({ payYearMonth: 1 })

CallHistorySchema.statics.createCallHistory = function(cid, callerPhoneNo, callerEmail, calleePhoneNo, calleeEmail, requestDate, startDate, endDate, duration, payYearMonth, payCompleted) {
    const callHistory = new this({
        cid,
        callerPhoneNo,
        callerEmail,
        calleePhoneNo,
        calleeEmail,
        requestDate,
        startDate,
        endDate,
        duration,
        //payYearMonth,
        payCompleted
    })
    
    // return the Promise
    return callHistory.save() 
}

CallHistorySchema.statics.getCallSummary = function(email, payYearMonth) {
    if (email) {
        return this.aggregate([
            {$match: {"payYearMonth": payYearMonth, "callerEmail":email}}, 
            {$project: {"callerEmail":1, "duration":1, "payYearMonth":1,"totalOfCharge": {$multiply:["$chargePerMinute", "$duration"]}}},
            {$group: {_id:"$callerEmail", payYearMonth: {$max: "$payYearMonth"}, totalOfCharge: {$sum: "$totalOfCharge"}, totalOfDuration: {$sum: "$duration"}, count: {$sum: 1}}},
            {$project: {"callerEmail":"$_id", "payYearMonth": 1, "totalOfCharge":"$totalOfCharge", "totalOfDuration":"$totalOfDuration", "count": "$count"}},
            {$sort: {"callerEmail":-1}}
        ]).exec()
    } else {
        return this.aggregate([
            {$match: {"payYearMonth": payYearMonth}}, 
            {$project: {"callerEmail":1, "duration":1, "payYearMonth":1,"totalOfCharge": {$multiply:["$chargePerMinute", "$duration"]}}},
            {$group: {_id:"$callerEmail", payYearMonth: {$max: "$payYearMonth"}, totalOfCharge: {$sum: "$totalOfCharge"}, totalOfDuration: {$sum: "$duration"}, count: {$sum: 1}}},
            {$project: {"callerEmail":"$_id", "payYearMonth": 1, "totalOfCharge":"$totalOfCharge", "totalOfDuration":"$totalOfDuration", "count": "$count"}},
            {$sort: {"callerEmail":-1}}
        ]).exec()
    }
}

// db.callhistories.aggregate([
//     {$match: {"payYearMonth": "201806"}}, 
//     {$project: {"callerEmail":1, "payYearMonth":1,"totalCharge": {$multiply:["$chargePerMinute", "$duration"]}}},
//     {$group: {_id:"$callerEmail", payYearMonth: {$max: "$payYearMonth"}, total: {$sum: "$totalCharge"}, count: {$sum: 1}}},
//     {$project: {"callerEmail":"$_id", "payYearMonth": 1, "total":"$total", "count": "$count"}},
//     {$sort: {"callerEmail":-1}}
// ])

CallHistorySchema.methods.completePayment = function() {
    this.payCompleted = 'Y'
    this.payDate = Date.now()

    return this.save()
}

module.exports = mongoose.model('CallHistory', CallHistorySchema)
